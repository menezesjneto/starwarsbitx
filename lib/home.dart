import 'dart:async';
import 'dart:math';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:starwarsbitx/detalhes_personagem_page.dart';
import 'package:starwarsbitx/models/personagem.dart';
import 'package:starwarsbitx/providers/api_provider.dart';
import 'package:starwarsbitx/services/page_transition.dart';
import 'package:starwarsbitx/widgets/theme.dart';
import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController searchController = new TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  double tamanhoAnimate = 0.9;
  double tamanhoAnimate2 = 0.5;
  double tamanhoAnimate3 = 0.7;
  double tamanhoAnimate5 = 1;
  int imgAtual = 0;
  bool alterarPosicao = false;
  bool _isStart = false;
  bool alterarIniciarParar = false;
  final _stopWatch = new Stopwatch();
  final _timeout = const Duration(seconds: 1);
  double sizeTimer = 35;
  bool alterarSizeTimer = true;
  bool typing = false;
  bool isLoading = false;
  bool isPause = false;
  List<Personagem> personagens = [];
  
  AssetsAudioPlayer audioPlayer = AssetsAudioPlayer.newPlayer();
  @override
  void initState() {
    super.initState();
    _startStopButtonPressed();
    audioPlayer.open(
      Audio("assets/audio/main_theme.mp3"),
      showNotification: false,
      autoStart: true
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.black,
        body: WillPopScope(
          onWillPop: () async{
            setState(() {
              typing = false;
              FocusScopeNode currentFocus = FocusScope.of(context);

              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            });
            return null;
          },
          child: GestureDetector(
            onTap: (){
              setState(() {
                typing = false;
                FocusScopeNode currentFocus = FocusScope.of(context);

                if (!currentFocus.hasPrimaryFocus) {
                  currentFocus.unfocus();
                }
              });
            },
            child: SingleChildScrollView(
              child: new Stack(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    decoration: new BoxDecoration(
                      image: new DecorationImage(
                          image: new AssetImage("assets/stars1.gif"),
                          fit: BoxFit.cover,
                          colorFilter:ColorFilter.mode(Colors.black54, BlendMode.darken)),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        alignment: Alignment.bottomCenter,
                        margin: EdgeInsets.only(left: 10, right: 10, top: 30),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            IconButton(
                              icon: Icon(FontAwesomeIcons.bars, color: Colors.white,),
                              onPressed: (){
                              _containerInfo(context: context);
                              },
                            ),
                            IconButton(
                              icon: Icon(isPause ? FontAwesomeIcons.volumeUp : FontAwesomeIcons.volumeMute, color: Colors.white,),
                              onPressed: (){
                                if(isPause){
                                  setState(() {
                                    isPause = false;
                                    audioPlayer.playOrPause();
                                  });
                                }else{
                                  setState(() {
                                    isPause = true;
                                    audioPlayer.playOrPause();
                                  });
                                }
                              },
                            ),
                          ],
                        ),
                      ),

                      Container(
                        height: 200,
                        child: FlareActor("assets/logo.flr",
                            alignment: Alignment.topCenter,
                            fit: BoxFit.contain,
                            animation: "main"),
                      ),

                      Container(
                        alignment: Alignment.topCenter,
                        child: Text(
                          "Digite o nome do seu personagem favorito de\nStar Wars!", textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.black, fontSize: 27, fontFamily: "Quicksand", fontWeight: FontWeight.bold, backgroundColor: CustomsColors.customYellow),
                        ),
                      ),

                      Divider(color: Colors.transparent),

                      Stack(
                        children: [
                          Container(
                            alignment: Alignment.center,
                            child: AnimatedContainer(
                              duration: Duration(seconds: 1),
                              width: alterarSizeTimer ? MediaQuery.of(context).size.width * 0.85 : MediaQuery.of(context).size.width * 0.90,
                              alignment: Alignment.center,
                              height: 120,
                              decoration: new BoxDecoration(
                                color: Colors.black.withOpacity(0.05),
                                  shape: BoxShape.rectangle,
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  border: Border.all(
                                      color: CustomsColors.customYellow, width: alterarSizeTimer ? 1.0 : 3.0)),
                            ),
                          ),

                          Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(
                              top: 10,
                            ),
                            child: AnimatedContainer(
                              duration: Duration(seconds: 1),
                              width: alterarSizeTimer ? MediaQuery.of(context).size.width * 0.84 : MediaQuery.of(context).size.width * 0.85,
                              alignment: Alignment.center,
                              child: Container(
                                padding: EdgeInsets.only(left: 15),
                                margin: EdgeInsets.only(left: 10),
                                width: MediaQuery.of(context).size.width*0.70,
                                height: 45,
                                alignment: Alignment.topCenter,
                                child: TextField(
                                  controller: searchController,
                                  decoration: new InputDecoration(
                                    border: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    errorBorder: InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                    hintText: "Personagem",
                                    hintStyle: new TextStyle(
                                      fontFamily: "Quicksand", fontSize: 22, color: typing ? Colors.white : CustomsColors.customYellow
                                    ),
                                    floatingLabelBehavior: FloatingLabelBehavior.never,
                                    labelStyle: new TextStyle(
                                      fontFamily: "Quicksand", fontSize: 18, color: CustomsColors.customYellow
                                    ),
                                    fillColor: Colors.white,
                                    //fillColor: Colors.green
                                  ),
                                  keyboardType: TextInputType.text,
                                  style: new TextStyle(
                                    fontFamily: "Quicksand", fontSize: 18, color: CustomsColors.customYellow
                                  ),
                                  onChanged: (text){
                                  },
                                  onTap: (){
                                    setState(() {
                                      typing = true;
                                    });
                                  },
                                  onEditingComplete: (){
                                    setState(() {
                                      typing = false;
                                    });
                                  },
                                ),
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                ),  
                              ),
                              height: 100,
                              decoration: new BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: typing ? Colors.red : Colors.red.withOpacity(0.3),
                                border: Border.all(
                                    color: Colors.red.withOpacity(0.6),
                                    width: alterarSizeTimer ? 1.0 : 2.0)),
                            ),
                          ),

                          Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(
                              top: 140,
                            ),
                            child: Container(
                              height: 70,
                              width: 300,
                              margin: EdgeInsets.only(top: 20),
                              child: RaisedButton(
                                elevation: 0,
                                color: searchController.text == "" ? CustomsColors.customYellow.withOpacity(0.3) : CustomsColors.customYellow,
                                child: isLoading ? Container(
                                  child: SpinKitWave(
                                    size: 25,
                                    color: Colors.red,
                                  ),
                                ) : Text(
                                  "Buscar", textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white, fontSize: searchController.text == "" ? 18 : 30, fontFamily: "Quicksand", fontWeight: FontWeight.bold ),
                                ),
                              onPressed: searchController.text == "" || isLoading ? (){} : () async {
                                setState(() {
                                  isLoading = true;
                                });
                                ApiProvider.getPersonagem(searchController.text.toLowerCase()).then((value){
                                  setState(() {
                                    if(value != null){
                                      personagens = value;
                                      if(value.isEmpty) showFloatingFlushbar(context);
                                      else{
                                        Navigator.push(context, SlideRightRoute(page: DetalhesPersonagemPage(personagens: personagens))).then((value) {
                                          setState(() {
                                            typing = false;
                                            FocusScopeNode currentFocus = FocusScope.of(context);

                                            if (!currentFocus.hasPrimaryFocus) {
                                              currentFocus.unfocus();
                                            }
                                          });
                                        })  ;
                                      }
                                    }else{
                                      _scaffoldKey.currentState.showSnackBar(
                                        SnackBar(
                                          content: Text("Opss! Algo de errado aconteceu", style: TextStyle(fontSize: 18.0),),
                                          backgroundColor: Colors.redAccent,
                                          duration: Duration(seconds: 1),
                                        )
                                      );
                                    }
                                    isLoading = false;
                                  });
                                });
                              },
                              ),
                            ),
                          ),
                        ],
                      ),

                      Divider(color: Colors.transparent),

                      Container(
                        alignment: Alignment.center,
                        child: Container(
                          height: 70,
                          width: 300,
                          margin: EdgeInsets.only(top: 20),
                          child: Text(
                            "Desenvolvido por José Neto\n\n02 de Setembro de 2020\nBITX Software House", textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white, fontSize: 12, fontFamily: "Quicksand", fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        )
      );
  }

  void _startTimeout() {
    new Timer(_timeout, _handleTimeout);
  }

  void _handleTimeout() {
    if (_stopWatch.isRunning) {
      _startTimeout();
    }
    setState(() {
      alterarSizeTimer ? sizeTimer = 45.0 : sizeTimer = 42.0;
      alterarSizeTimer = !alterarSizeTimer;
    });
    _alterarFundoAleatorio();
  }

  void _startStopButtonPressed() {
    setState(() {
      alterarIniciarParar = !alterarIniciarParar;
      if (_stopWatch.isRunning) {
        _isStart = true;
        _stopWatch.stop();
      } else {
        _isStart = false;
        _stopWatch.start();
        _startTimeout();
      }
    });
  }
  _alterarFundoAleatorio() {
    setState(() {
      imgAtual++;
      if (imgAtual == 4) imgAtual = 0;
      int value = imgAtual;
      switch (value) {
        case 0:
          tamanhoAnimate = 1;
          tamanhoAnimate2 = 0;
          tamanhoAnimate3 = 0;
          tamanhoAnimate5 = 0;
          imgAtual = 0;
          break;
        case 1:
          tamanhoAnimate2 = 0.9;
          tamanhoAnimate = 0;
          tamanhoAnimate3 = 0;
          tamanhoAnimate5 = 0;
          imgAtual = 1;
          break;
      }
    });
  }

  void _containerInfo<T>({BuildContext context, String tipo}) {
    showCupertinoModalPopup<T>(
      context: context,
      builder: (BuildContext ctx) =>
        CupertinoActionSheet(
          title: const Text('Star Wars BITX', style: TextStyle(fontSize: 18, fontFamily: "Quicksand", fontWeight: FontWeight.bold),),
          message: const Text(
            "Desenvolvido por José Neto\n02 de Setembro de 2020\n\nDesafio BitX Mobile / Flutter", textAlign: TextAlign.center,
            style: TextStyle(color: Colors.black, fontSize: 14, fontFamily: "Quicksand", fontWeight: FontWeight.bold),
          ),
          actions: <Widget>[
            CupertinoActionSheetAction(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.instagramSquare, size: 17,),
                  VerticalDivider(color: Colors.transparent),
                  Text(
                    "@menezesjneto", textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              onPressed: () async{
                const url = 'https://www.instagram.com/menezesjneto';
                if (await canLaunch(url)) {
                  await launch(url);
                } else {
                  throw 'Could not launch $url';
                }
              },
            ),
            CupertinoActionSheetAction(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.mailBulk, size: 17,),
                  VerticalDivider(color: Colors.transparent),
                  Text(
                    "menezesneto13@hotmail.com", textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              onPressed: () {
              },
            ),
          ],
          cancelButton: CupertinoActionSheetAction(
            child: const Text('Fechar', style: TextStyle(color: Colors.red),),
            isDefaultAction: true,
            onPressed: () {
              Navigator.pop(ctx, '');
            },
          )
        ),
    );
  }

  void showFloatingFlushbar(BuildContext context) {
  Flushbar(
    duration: Duration(milliseconds: 4000),
    margin: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 150),
    padding: EdgeInsets.only(top: 30, bottom: 30),
    backgroundGradient: LinearGradient(
      colors: [Colors.black, Colors.black],
    ),
    shouldIconPulse: true,
    dismissDirection: FlushbarDismissDirection.HORIZONTAL,
    flushbarPosition: FlushbarPosition.BOTTOM,
    forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
    titleText: Text('Desculpe, não encontramos nenhum personagem com esse nome :(', style: TextStyle(color: Colors.white, fontFamily: "Quicksand", fontSize: 16),),
    flushbarStyle: FlushbarStyle.GROUNDED,
    messageText :Text('QUE A FORÇA ESTEJA COM VOCÊ', style: TextStyle(color: CustomsColors.customYellow, fontFamily: "Quicksand", fontSize: 18),),
    icon: Icon(FontAwesomeIcons.handSpock, color: CustomsColors.customYellow, size: 30,),
  )..show(context);
 }
}
