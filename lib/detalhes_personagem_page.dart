import 'dart:async';
import 'dart:math';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:expandable/expandable.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:starwarsbitx/models/films.dart';
import 'package:starwarsbitx/models/personagem.dart';
import 'package:starwarsbitx/models/starships.dart';
import 'package:starwarsbitx/providers/api_provider.dart';
import 'package:starwarsbitx/widgets/theme.dart';
import 'package:url_launcher/url_launcher.dart';

import 'models/vehicles.dart';

class DetalhesPersonagemPage extends StatefulWidget {
  final List<Personagem> personagens;
  DetalhesPersonagemPage({@required this.personagens});

  @override
  _DetalhesPersonagemPageState createState() => _DetalhesPersonagemPageState();
}

class _DetalhesPersonagemPageState extends State<DetalhesPersonagemPage> {
  TextEditingController searchController = new TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  double tamanhoAnimate = 0.9;
  double tamanhoAnimate2 = 0.5;
  double tamanhoAnimate3 = 0.7;
  double tamanhoAnimate5 = 1;
  int imgAtual = 0;
  bool alterarPosicao = false;
  bool _isStart = false;
  bool alterarIniciarParar = false;
  final _stopWatch = new Stopwatch();
  final _timeout = const Duration(seconds: 1);
  double sizeTimer = 35;
  bool alterarSizeTimer = true;
  bool typing = false;
  bool isLoading = false;
  bool isPause = false;
  List<Personagem> personagens = [];

  List<Films> films = [];
  List<Starship> starship = [];
  List<Vehicle> vehicles = [];

  List<Map<String, bool>> mapLoadings = [];
  
  AssetsAudioPlayer audioPlayer = AssetsAudioPlayer.newPlayer();
  @override
  void initState() {
    super.initState();
    for (var i = 0; i < widget.personagens.length; i++) {
      mapLoadings.add({
        "filmsLoading": false,
        "starships": false,
        "vehiclesLoading": false,
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.black,
        body: WillPopScope(
          onWillPop: () async{
            Navigator.pop(context);
            return null;
          },
          child: SingleChildScrollView(
                child: new Stack(
                  children: <Widget>[
                    Column(
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          decoration: new BoxDecoration(
                            color: Colors.black45
                          ),
                        ),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          alignment: Alignment.bottomLeft,
                          margin: EdgeInsets.only(left: 10, right: 10, top: 30),
                          child: IconButton(
                                icon: Icon(Icons.arrow_back, color: Colors.white, size: 25,),
                                onPressed: (){
                                  Navigator.pop(context);
                                },
                              ),
                        ),

                        Stack(
                          children: [
                            Container(
                              height: 150,
                              child: FlareActor("assets/star.flr",
                                  alignment: Alignment.topCenter,
                                  fit: BoxFit.cover,
                                  animation: "Placeholder"),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 30),
                              alignment: Alignment.topCenter,
                              child: Text("Personagens", textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.black, fontSize: 33, fontFamily: "Quicksand", fontWeight: FontWeight.bold, backgroundColor: CustomsColors.customYellow),
                              ),
                            ),
                          ],
                        ),

                        Divider(color: Colors.transparent),

                        Container(
                          alignment: Alignment.center,
                          child: Text("Clique em um personagem para exibir as suas informações", textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white, fontSize: 12, fontFamily: "Quicksand"),
                          ),
                        ),

                        Container(
                          margin: EdgeInsets.only(left: 20, right: 20),
                          child: _createList(context),
                        ),
                  ],
              )])),
          ));
  }

  Widget _createList(context) {
    return ListView.builder(
        primary: false,
        shrinkWrap: true,
        //padding: EdgeInsets.all(10.0),
        itemCount: widget.personagens.length,
        itemBuilder: (context, index) {
          return Container(
            padding: EdgeInsets.only(bottom: 15),
            child: ExpandablePanel(
                //controller: expandableController,
                header: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(FontAwesomeIcons.handSpock, color: CustomsColors.customYellow),
                    VerticalDivider(color: Colors.transparent, width: 15,),
                    Container(
                      height: 80,
                      alignment: Alignment.centerLeft,
                      width: MediaQuery.of(context).size.width*0.7,
                      child: Container(
                        child: Text(widget.personagens[index].name, style: TextStyle(fontSize: 18, color: Colors.white, fontFamily: "Quicksand"),
                       textAlign: TextAlign.start, overflow: TextOverflow.ellipsis),
                      ),
                    ),
                  ],
                ),
                iconColor: CustomsColors.customYellow,
                expanded: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text("Nome:  ", style: TextStyle(fontSize: 16, color: CustomsColors.customYellow, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                        Text(widget.personagens[index].name, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                      ],
                    ),
                    Row(
                      children: [
                        Text("Altura:  ", style: TextStyle(fontSize: 16, color: CustomsColors.customYellow, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                        Text(widget.personagens[index].height + " cm", style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                      ],
                    ),
                    Row(
                      children: [
                        Text("Peso:  ", style: TextStyle(fontSize: 16, color: CustomsColors.customYellow, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                        Text(widget.personagens[index].mass + " Kg", style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                      ],
                    ),
                    Row(
                      children: [
                        Text("Cor do cabelo:  ", style: TextStyle(fontSize: 16, color: CustomsColors.customYellow, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                        Text(widget.personagens[index].hairColor, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                      ],
                    ),
                    Row(
                      children: [
                        Text("Cor da Pele:  ", style: TextStyle(fontSize: 16, color: CustomsColors.customYellow, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                        Text(widget.personagens[index].skinColor, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                      ],
                    ),
                    Row(
                      children: [
                        Text("Cor dos olhos:  ", style: TextStyle(fontSize: 16, color: CustomsColors.customYellow, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                        Text(widget.personagens[index].eyeColor, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                      ],
                    ),
                    Row(
                      children: [
                        Text("Ano de nascimento:  ", style: TextStyle(fontSize: 16, color: CustomsColors.customYellow, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                        Text(widget.personagens[index].birthYear, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                      ],
                    ),
                    Row(
                      children: [
                        Text("Gênero:  ", style: TextStyle(fontSize: 16, color: CustomsColors.customYellow, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                        Text(widget.personagens[index].gender, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                      ],
                    ),
                    widget.personagens[index].films.isEmpty ? Container() : films.isEmpty ? Container(
                      alignment: Alignment.center,
                      child: Container(
                        height: 70,
                        width: 300,
                        margin: EdgeInsets.only(top: 20),
                        child: RaisedButton(
                          elevation: 0,
                          color: Colors.red,
                          child: mapLoadings[index]["filmsLoading"] ? Container(
                            child: SpinKitWave(
                              size: 25,
                              color: CustomsColors.customYellow,
                            ),
                          ) : Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Filmes", textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white, fontSize: 16, fontFamily: "Quicksand", fontWeight: FontWeight.bold ),
                              ),
                              Icon(Icons.arrow_right, color: Colors.white,),

                            ],
                          ),
                        onPressed: mapLoadings[index]["filmsLoading"] ? (){} : () async {
                          setState(() {
                            films.clear();
                            mapLoadings[index]["filmsLoading"] = true;
                          });
                          for (var i = 0; i < widget.personagens[index].films.length; i++) {
                            await ApiProvider.getFilmsVeiculosNaves(widget.personagens[index].films[i], "filme").then((value){
                            setState(() {
                              if(value != null){
                                films.add(value);
                                print(value);
                                //exibe
                              }else{
                                mapLoadings[index]["filmsLoading"] = false;
                                _scaffoldKey.currentState.showSnackBar(
                                  SnackBar(
                                    content: Text("Opss! Algo de errado aconteceu", style: TextStyle(fontSize: 18.0),),
                                    backgroundColor: Colors.redAccent,
                                    duration: Duration(seconds: 2),
                                  )
                                );
                              }
                            });
                          });
                          if(films.length == widget.personagens[index].films.length)
                            setState(() {
                              mapLoadings[index]["filmsLoading"] = false;
                            });
                          }
                          
                        },
                        ),
                      ),
                    ) : Container(
                      child: Column(
                        children: [
                          Text(
                            "Filmes", textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white, fontSize: 16, fontFamily: "Quicksand", fontWeight: FontWeight.bold ),
                          ),
                          _createListFilms(context),
                        ],
                      ),
                    ),

                    widget.personagens[index].starships.isEmpty ? Container() : starship.isEmpty ? Container(
                      alignment: Alignment.center,
                      child: Container(
                        height: 70,
                        width: 300,
                        margin: EdgeInsets.only(top: 20),
                        child: RaisedButton(
                          elevation: 0,
                          color: Colors.green,
                          child: mapLoadings[index]["starships"] ? Container(
                            child: SpinKitWave(
                              size: 25,
                              color: CustomsColors.customYellow,
                            ),
                          ) : Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Naves Espaciais", textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white, fontSize: 16, fontFamily: "Quicksand", fontWeight: FontWeight.bold ),
                              ),
                              Icon(Icons.arrow_right, color: Colors.white,),

                            ],
                          ),
                        onPressed: mapLoadings[index]["starships"] ? (){} : () async {
                          print(widget.personagens[index].starships);
                          setState(() {
                            starship.clear();
                            mapLoadings[index]["starships"] = true;
                          });
                          for (var i = 0; i < widget.personagens[index].starships.length; i++) {
                            await ApiProvider.getFilmsVeiculosNaves(widget.personagens[index].starships[i], "nave").then((value){
                              print("aquiii $value.name");
                            setState(() {
                              if(value != null){
                                starship.add(value);
                                print(value);
                                //exibe
                              }else{
                                mapLoadings[index]["starships"] = false;
                                _scaffoldKey.currentState.showSnackBar(
                                  SnackBar(
                                    content: Text("Opss! Algo de errado aconteceu", style: TextStyle(fontSize: 18.0),),
                                    backgroundColor: Colors.redAccent,
                                    duration: Duration(seconds: 2),
                                  )
                                );
                              }
                              
                            });
                            if(starship.length == widget.personagens[index].starships.length)
                            setState(() {
                              mapLoadings[index]["starships"] = false;
                            });
                          });
                          
                          }
                        },
                        ),
                      ),
                    ) : Container(
                      child: Column(
                        children: [
                          Divider(color: Colors.transparent),
                          Text(
                            "Naves Espaciais", textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white, fontSize: 16, fontFamily: "Quicksand", fontWeight: FontWeight.bold ),
                          ),
                          _createListStarships(context),
                        ],
                      ),
                    ),

                    widget.personagens[index].vehicles.isEmpty ? Container() : vehicles.isEmpty ? Container(
                      alignment: Alignment.center,
                      child: Container(
                        height: 70,
                        width: 300,
                        margin: EdgeInsets.only(top: 20),
                        child: RaisedButton(
                          elevation: 0,
                          color: Colors.blue,
                          child: mapLoadings[index]["vehiclesLoading"] ? Container(
                            child: SpinKitWave(
                              size: 25,
                              color: CustomsColors.customYellow,
                            ),
                          ) : Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Veículos", textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white, fontSize: 16, fontFamily: "Quicksand", fontWeight: FontWeight.bold ),
                              ),
                              Icon(Icons.arrow_right, color: Colors.white,),

                            ],
                          ),
                        onPressed: mapLoadings[index]["vehiclesLoading"] ? (){} : () async {
                          print(widget.personagens[index].vehicles);
                          setState(() {
                            vehicles.clear();
                            mapLoadings[index]["vehiclesLoading"] = true;
                          });
                          for (var i = 0; i < widget.personagens[index].vehicles.length; i++) {
                            await ApiProvider.getFilmsVeiculosNaves(widget.personagens[index].vehicles[i], "veiculo").then((value){
                              print("aquiii $value");
                            setState(() {
                              if(value != null){
                                vehicles.add(value);
                                print(value);
                                //exibe
                              }else{
                                mapLoadings[index]["vehiclesLoading"] = false;
                                _scaffoldKey.currentState.showSnackBar(
                                  SnackBar(
                                    content: Text("Opss! Algo de errado aconteceu", style: TextStyle(fontSize: 18.0),),
                                    backgroundColor: Colors.redAccent,
                                    duration: Duration(seconds: 2),
                                  )
                                );
                              }
                              
                            });
                            if(vehicles.length == widget.personagens[index].vehicles.length)
                            setState(() {
                              mapLoadings[index]["vehiclesLoading"] = false;
                            });
                          });
                          
                          }
                        },
                        ),
                      ),
                    ) : Container(
                      child: Column(
                        children: [
                          Divider(color: Colors.transparent),
                          Text(
                            "Veículos", textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white, fontSize: 16, fontFamily: "Quicksand", fontWeight: FontWeight.bold ),
                          ),
                          _createListVehicles(context),
                        ],
                      ),
                    ),
                    
                  ],
                ),
              ),
            );
        },
      );
  }

  Widget _createListFilms(context) {
    return ListView.builder(
        primary: false,
        shrinkWrap: true,
        //padding: EdgeInsets.all(10.0),
        itemCount: films.length,
        itemBuilder: (context, index) {
          return Container(
            child: ExpandablePanel(
                //controller: expandableController,
                header: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(FontAwesomeIcons.film, color: Colors.red),
                    VerticalDivider(color: Colors.transparent, width: 15,),
                    Container(
                      height: 80,
                      alignment: Alignment.centerLeft,
                      width: MediaQuery.of(context).size.width*0.7,
                      child: Container(
                        child: Text(films[index].title, style: TextStyle(fontSize: 18, color: Colors.red, fontFamily: "Quicksand"),
                       textAlign: TextAlign.start, overflow: TextOverflow.ellipsis),
                      ),
                    ),
                  ],
                ),
                iconColor: Colors.red,
                expanded: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Título:  ", style: TextStyle(fontSize: 16, color: Colors.red, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(films[index].title, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Opening Crawl:  ", style: TextStyle(fontSize: 16, color: Colors.red, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(films[index].openingCrawl.replaceAll("\r", "").replaceAll("\n", ""), style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Diretor:  ", style: TextStyle(fontSize: 16, color: Colors.red, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(films[index].director, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Produtor:  ", style: TextStyle(fontSize: 16, color: Colors.red, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(films[index].producer, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Data de lançamento:  ", style: TextStyle(fontSize: 16, color: Colors.red, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(films[index].releaseDate, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                  ],
                ),
              ),
            );
        },
      );
  }

  Widget _createListStarships(context) {
    return ListView.builder(
        primary: false,
        shrinkWrap: true,
        //padding: EdgeInsets.all(10.0),
        itemCount: starship.length,
        itemBuilder: (context, index) {
          return Container(
            child: ExpandablePanel(
                //controller: expandableController,
                header: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(FontAwesomeIcons.spaceShuttle, color: Colors.green),
                    VerticalDivider(color: Colors.transparent, width: 15,),
                    Container(
                      height: 80,
                      alignment: Alignment.centerLeft,
                      width: MediaQuery.of(context).size.width*0.7,
                      child: Container(
                        child: Text(starship[index].name, style: TextStyle(fontSize: 18, color: Colors.green, fontFamily: "Quicksand"),
                       textAlign: TextAlign.start, overflow: TextOverflow.ellipsis),
                      ),
                    ),
                  ],
                ),
                iconColor: Colors.green,
                expanded: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Nome:  ", style: TextStyle(fontSize: 16, color: Colors.green, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(starship[index].name, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Modelo:  ", style: TextStyle(fontSize: 16, color: Colors.green, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(starship[index].model, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Marca:  ", style: TextStyle(fontSize: 16, color: Colors.green, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(starship[index].manufacturer, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Cost in credits:  ", style: TextStyle(fontSize: 16, color: Colors.green, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(starship[index].costInCredits, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Tamanho:  ", style: TextStyle(fontSize: 16, color: Colors.green, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(starship[index].length, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Velocidade atmosférica máxima:  ", style: TextStyle(fontSize: 16, color: Colors.green, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(starship[index].maxAtmospheringSpeed, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Equipe técnica:  ", style: TextStyle(fontSize: 16, color: Colors.green, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(starship[index].crew, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Passageiros:  ", style: TextStyle(fontSize: 16, color: Colors.green, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(starship[index].passengers, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Capacidade de carga:  ", style: TextStyle(fontSize: 16, color: Colors.green, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                        Text(starship[index].cargoCapacity, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Consumíveis:  ", style: TextStyle(fontSize: 16, color: Colors.green, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(starship[index].consumables, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Avaliação hiperdrive:  ", style: TextStyle(fontSize: 16, color: Colors.green, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(starship[index].hyperdriveRating, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("MGLT:  ", style: TextStyle(fontSize: 16, color: Colors.green, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(starship[index].mglt, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Classe:  ", style: TextStyle(fontSize: 16, color: Colors.green, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                        Text(starship[index].starshipClass, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                  ],
                ),
              ),
            );
        },
      );
  }

  Widget _createListVehicles(context) {
    return ListView.builder(
        primary: false,
        shrinkWrap: true,
        //padding: EdgeInsets.all(10.0),
        itemCount: vehicles.length,
        itemBuilder: (context, index) {
          return Container(
            child: ExpandablePanel(
                //controller: expandableController,
                header: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(FontAwesomeIcons.rocket, color: Colors.green),
                    VerticalDivider(color: Colors.transparent, width: 15,),
                    Container(
                      height: 80,
                      alignment: Alignment.centerLeft,
                      width: MediaQuery.of(context).size.width*0.7,
                      child: Container(
                        child: Text(vehicles[index].name, style: TextStyle(fontSize: 18, color: Colors.green, fontFamily: "Quicksand"),
                       textAlign: TextAlign.start, overflow: TextOverflow.ellipsis),
                      ),
                    ),
                  ],
                ),
                iconColor: Colors.green,
                expanded: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Nome:  ", style: TextStyle(fontSize: 16, color: Colors.blue, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(vehicles[index].name, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Modelo:  ", style: TextStyle(fontSize: 16, color: Colors.blue, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(vehicles[index].model, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Marca:  ", style: TextStyle(fontSize: 16, color: Colors.blue, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(vehicles[index].manufacturer, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Cost in credits:  ", style: TextStyle(fontSize: 16, color: Colors.blue, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(vehicles[index].costInCredits, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Tamanho:  ", style: TextStyle(fontSize: 16, color: Colors.blue, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(vehicles[index].length, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Velocidade atmosférica máxima:  ", style: TextStyle(fontSize: 16, color: Colors.blue, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(vehicles[index].maxAtmospheringSpeed, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Equipe técnica:  ", style: TextStyle(fontSize: 16, color: Colors.blue, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(vehicles[index].crew, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Passageiros:  ", style: TextStyle(fontSize: 16, color: Colors.blue, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(vehicles[index].passengers, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Capacidade de carga:  ", style: TextStyle(fontSize: 16, color: Colors.blue, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                        Text(vehicles[index].cargoCapacity, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Consumíveis:  ", style: TextStyle(fontSize: 16, color: Colors.blue, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                        Text(vehicles[index].consumables, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Classe:  ", style: TextStyle(fontSize: 16, color: Colors.blue, fontFamily: "Quicksand"), textAlign: TextAlign.center),
                        Text(vehicles[index].vehicleClass, style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Quicksand"), textAlign: TextAlign.start),
                      ],
                    ),
                  ],
                ),
              ),
            );
        },
      );
  }


  void showFloatingFlushbar(BuildContext context) {
  Flushbar(
    duration: Duration(milliseconds: 4000),
    margin: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 150),
    padding: EdgeInsets.only(top: 30, bottom: 30),
    backgroundGradient: LinearGradient(
      colors: [Colors.black, Colors.black],
    ),
    shouldIconPulse: true,
    dismissDirection: FlushbarDismissDirection.HORIZONTAL,
    flushbarPosition: FlushbarPosition.BOTTOM,
    forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
    titleText: Text('Desculpe, não encontramos nenhum personagem com esse nome :(', style: TextStyle(color: Colors.white, fontFamily: "Quicksand", fontSize: 16),),
    flushbarStyle: FlushbarStyle.GROUNDED,
    messageText :Text('QUE A FORÇA ESTEJA COM VOCÊ', style: TextStyle(color: CustomsColors.customYellow, fontFamily: "Quicksand", fontSize: 18),),
    icon: Icon(FontAwesomeIcons.handSpock, color: CustomsColors.customYellow, size: 30,),
  )..show(context);
 }
}
