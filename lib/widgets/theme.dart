import 'package:flutter/material.dart';
  
class CustomsColors {

 static final MaterialColor customYellow = const MaterialColor(
    0xFFEEDB00,
    const <int, Color>{
      50: const Color(0xFFEEDB00),
      100: const Color(0xFFEEDB00),
      200: const Color(0xFFEEDB00),
      300: const Color(0xFFEEDB00),
      400: const Color(0xFFEEDB00),
      500: const Color(0xFFEEDB00),
      600: const Color(0xFFEEDB00),
      700: const Color(0xFFEEDB00),
      800: const Color(0xFFEEDB00),
      900: const Color(0xFFEEDB00),
    },
  );


}