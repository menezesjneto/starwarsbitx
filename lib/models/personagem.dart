import 'dart:convert';

class Personagem {
  String name;
  String height;
  String mass;
  String hairColor;
  String skinColor;
  String eyeColor;
  String birthYear;
  String gender;
  String homeworld;
  String created;
  String edited;
  String url;
  List<dynamic> films = [];
  List<dynamic> species = [];
  List<dynamic> vehicles = [];
  List<dynamic> starships = [];
  Personagem({
    this.name,
    this.height,
    this.mass,
    this.hairColor,
    this.skinColor,
    this.eyeColor,
    this.birthYear,
    this.gender,
    this.homeworld,
    this.created,
    this.edited,
    this.url,
    this.films,
    this.species,
    this.starships,
    this.vehicles
  });

  factory Personagem.fromJson(Map<dynamic, dynamic> parsedJson){
    return Personagem(
      name: parsedJson["name"],
      height: parsedJson["height"],
      mass: parsedJson["mass"],
      hairColor: parsedJson["hair_color"],
      skinColor: parsedJson["skin_color"],
      eyeColor: parsedJson["eye_color"],
      birthYear: parsedJson["birth_year"],
      gender: parsedJson["gender"],
      homeworld: parsedJson["homeworld"],
      created: parsedJson["created"],
      edited: parsedJson["edited"],
      url: parsedJson["url"],
      films: parsedJson["films"],
      species: parsedJson["species"],
      starships: parsedJson["starships"],
      vehicles: parsedJson["vehicles"],
    );
  }

  Map<String, dynamic> toJson() => {
    "name": name,
    "height": height,
    "mass": mass,
    "average_height": hairColor,
    "skin_colors": skinColor,
    "eye_colors": eyeColor,
    "average_lifespan": birthYear,
    "gender": gender,
    "homeworld": homeworld,
    "created": created,
    "edited": edited,
    "url": url,
    "films": films,
    "species": species,
    "starships": starships,
    "vehicles": vehicles,
  };

    static List<Personagem> listFromString(String str) => new List<Personagem>.from(
    json.decode(str).map((x) => Personagem.fromJson(x)));

  static List<Personagem> listFromJson(List<dynamic> isAtivo) {
    return isAtivo.map((post) => Personagem.fromJson(post)).toList();
  }

  static String listAvaliacaoToJson(List<Personagem> isAtivo) =>
    json.encode(new List<dynamic>.from(isAtivo.map((x) => x.toJson())));
  }