import 'dart:convert';

class Films {
  String title;
  int episodeId;
  String openingCrawl;
  String director;
  String producer;
  String releaseDate;
  String created;
  String edited;
  String url;

  Films({
    this.title,
    this.episodeId,
    this.openingCrawl,
    this.director,
    this.producer,
    this.releaseDate,
    this.created,
    this.edited,
    this.url,

  });

  factory Films.fromJson(Map<dynamic, dynamic> parsedJson){
    return Films(
      title: parsedJson["title"],
      episodeId: int.parse(parsedJson["episode_id"].toString()),
      openingCrawl: parsedJson["opening_crawl"],
      director: parsedJson["director"],
      producer: parsedJson["producer"],
      releaseDate: parsedJson["release_date"],
      created: parsedJson["created"],
      edited: parsedJson["edited"],
      url: parsedJson["url"],
    );
  }

    static List<Films> listFromString(String str) => new List<Films>.from(
    json.decode(str).map((x) => Films.fromJson(x)));

  static List<Films> listFromJson(List<dynamic> isAtivo) {
    return isAtivo.map((post) => Films.fromJson(post)).toList();
  }
  
  }