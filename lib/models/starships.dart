import 'dart:convert';

class Starship {
  String name;
  String model;
  String manufacturer;
  String costInCredits;
  String length;
  String maxAtmospheringSpeed;
  String crew;
  String passengers;
  String cargoCapacity;
  String consumables;
  String hyperdriveRating;
  String starshipClass;
  String mglt;
  String created;
  String edited;
  String url;

  Starship({
    this.name,
    this.model,
    this.manufacturer,
    this.costInCredits,
    this.length,
    this.maxAtmospheringSpeed,
    this.created,
    this.edited,
    this.url,
    this.crew,
    this.passengers,
    this.cargoCapacity,
    this.consumables,
    this.hyperdriveRating,
    this.starshipClass,
    this.mglt

  });

  factory Starship.fromJson(Map<dynamic, dynamic> parsedJson){
    return Starship(
      name: parsedJson["name"],
      model: parsedJson["model"],
      manufacturer: parsedJson["manufacturer"],
      costInCredits: parsedJson["cost_in_credits"],
      length: parsedJson["length"],
      maxAtmospheringSpeed: parsedJson["max_atmosphering_speed"],
      crew: parsedJson["crew"],
      passengers: parsedJson["passengers"],
      cargoCapacity: parsedJson["cargo_capacity"],
      consumables: parsedJson["consumables"],
      hyperdriveRating: parsedJson["hyperdrive_rating"],
      mglt: parsedJson["MGLT"],
      starshipClass: parsedJson["starship_class"],
      created: parsedJson["created"],
      edited: parsedJson["edited"],
      url: parsedJson["url"],
    );
  }


    static List<Starship> listFromString(String str) => new List<Starship>.from(
    json.decode(str).map((x) => Starship.fromJson(x)));

  static List<Starship> listFromJson(List<dynamic> isAtivo) {
    return isAtivo.map((post) => Starship.fromJson(post)).toList();
  }

  }