import 'dart:convert';

class Vehicle {
  String name;
  String model;
  String manufacturer;
  String costInCredits;
  String length;
  String maxAtmospheringSpeed;
  String crew;
  String passengers;
  String cargoCapacity;
  String consumables;
  String vehicleClass;
  String created;
  String edited;
  String url;

  Vehicle({
    this.name,
    this.model,
    this.manufacturer,
    this.costInCredits,
    this.length,
    this.maxAtmospheringSpeed,
    this.created,
    this.edited,
    this.url,
    this.crew,
    this.passengers,
    this.cargoCapacity,
    this.consumables,
    this.vehicleClass,

  });

  factory Vehicle.fromJson(Map<dynamic, dynamic> parsedJson){
    return Vehicle(
      name: parsedJson["name"],
      model: parsedJson["model"],
      manufacturer: parsedJson["manufacturer"],
      costInCredits: parsedJson["cost_in_credits"],
      length: parsedJson["length"],
      maxAtmospheringSpeed: parsedJson["max_atmosphering_speed"],
      crew: parsedJson["crew"],
      passengers: parsedJson["passengers"],
      cargoCapacity: parsedJson["cargo_capacity"],
      consumables: parsedJson["consumables"],
      vehicleClass: parsedJson["vehicle_class"],
      created: parsedJson["created"],
      edited: parsedJson["edited"],
      url: parsedJson["url"],
    );
  }


    static List<Vehicle> listFromString(String str) => new List<Vehicle>.from(
    json.decode(str).map((x) => Vehicle.fromJson(x)));

  static List<Vehicle> listFromJson(List<dynamic> isAtivo) {
    return isAtivo.map((post) => Vehicle.fromJson(post)).toList();
  }

  }