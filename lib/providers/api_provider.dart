import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:http/http.dart' as http;
import 'package:starwarsbitx/models/films.dart';
import 'package:starwarsbitx/models/personagem.dart';
import 'package:starwarsbitx/models/starships.dart';
import 'package:starwarsbitx/models/vehicles.dart';


class ApiProvider {

  static final ApiProvider _singleton = new ApiProvider._internal();
  factory ApiProvider() {
    return _singleton;
  }
  ApiProvider._internal();

  static final String urlApi = "https://swapi.dev/api/";
  


  static Future<dynamic> getPersonagem(String name) async {
    try {

      String url = urlApi + "people/?search=$name";

      final response =  await http.get(
        url, 
      );

      List<Personagem> personagens = [];
      var res = json.decode(response.body);
      for (var i = 0; i < res['results'].length; i++) {
        personagens.add(Personagem.fromJson(res['results'][i]));
      }
      print(personagens);
      return personagens;
    } catch(e) {
      print(e);
     return null;
    }
  }

  static Future<dynamic> getFilmsVeiculosNaves(String urlFilmsVeiculosNaves, String filmeOrVeiculoOrNaves) async {
    try {
      final response =  await http.get(
        urlFilmsVeiculosNaves, 
      );
      print(response.body);
      if(filmeOrVeiculoOrNaves == "filme"){
        Films film;
        var res = json.decode(response.body);
        film = Films.fromJson(res);
        return film;
      }else if(filmeOrVeiculoOrNaves == "nave"){
        Starship starship;
        var res = json.decode(response.body);
        starship = Starship.fromJson(res);
        print(starship.name);
        return starship;
      }else{
        Vehicle vehicle;
        var res = json.decode(response.body);
        vehicle = Vehicle.fromJson(res);
        return vehicle;
      }

    } catch(e) {
      print("exception $e");
     return null;
    }
  }


}